﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Mvc.Facebook;
using Microsoft.AspNet.Mvc.Facebook.Client;
using AppharborDemo.Models;
using Elmah;
using System;

namespace AppharborDemo.Controllers
{
    public class ElmahHandledErrorLoggerFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            // Log only handled exceptions, because all other will be caught by ELMAH anyway.
            if (context.ExceptionHandled)
                ErrorSignal.FromCurrentContext().Raise(context.Exception);
        }
    }

    public class HomeController : Controller
    {
        db6a395d5cf1ed40a8bf72a287006121c5Entities db = new db6a395d5cf1ed40a8bf72a287006121c5Entities();
        [FacebookAuthorize("email", "user_photos")]
        public async Task<ActionResult> Index(FacebookContext context)
        {
            if (ModelState.IsValid)
            {
                var user = await context.Client.GetCurrentUserAsync<MyAppUser>();

                List<FACEBOOK_USER> users = db.FACEBOOK_USER.ToList();
                ViewBag.FacebookUsers = users;
                ViewBag.FacebookGroups = new SelectList(db.FACEBOOK_GROUP, "ID", "NAME");

                //Giả lập lỗi
                //throw new Exception("Giả lập lỗi");
                return View(user);
            }

            return View("Error");
        }

        public ActionResult AddUser(FACEBOOK_USER user)
        {
            try
            {
                db.FACEBOOK_USER.Add(user);
                db.SaveChanges();
                string result = user.NAME + " - " + db.FACEBOOK_GROUP.First(g => g.ID == user.ID_GROUP).NAME;
                return Content(result);
            }
            catch
            {
            }
            return Content("Failed");
        }

        // This action will handle the redirects from FacebookAuthorizeFilter when 
        // the app doesn't have all the required permissions specified in the FacebookAuthorizeAttribute.
        // The path to this action is defined under appSettings (in Web.config) with the key 'Facebook:AuthorizationRedirectPath'.
        public ActionResult Permissions(FacebookRedirectContext context)
        {
            if (ModelState.IsValid)
            {
                return View(context);
            }

            return View("Error");
        }

    }
}
